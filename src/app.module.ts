import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [UsersModule, AuthModule, ScheduleModule.forRoot() , ConfigModule.forRoot({ envFilePath: '.env'},
  ), MongooseModule.forRoot('mongodb://'+ process.env.MONGODB_HOST + '/' + process.env.MONGODB_DB_NAME)],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
 