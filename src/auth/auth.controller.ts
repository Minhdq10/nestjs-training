import { Controller, UseGuards, HttpStatus, Response, Request, Get, Post, Body, Put, Param, Delete, Logger} from '@nestjs/common';
import { CreateUserDto } from '../users/dto/createUser.dto';
import { LoginUserDto } from '../users/dto/loginUser.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport'; 
import { ApiTags ,ApiResponse } from '@nestjs/swagger';
import { UsersService } from '../users/users.service';
import { IUser } from 'src/users/interfaces/user.interface';
import { JwtAuthGuard } from './passport/jwt-auth.guard';
import { Cron } from '@nestjs/schedule';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService,
        private readonly usersService: UsersService,) {} 
        
    private readonly logger = new Logger(AuthController.name);

    @Post('login')
    @UseGuards(AuthGuard('local'))
    public async login(@Response() res, @Body() login: LoginUserDto){
        return await this.usersService.findOne({ username: login.email}).then(user => {
            if (!user) {
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    message: 'User Not Found',
                });
            } else {
                console.log('start getting the token');
                const token = this.authService.createToken(user);
                console.log(token);
                return res.status(HttpStatus.OK).json(token);
            }
        });
    }

    @Get()
    @UseGuards(JwtAuthGuard)
    async findAll(): Promise<IUser[]> {
      return await this.usersService.findAll();
    }

    @Post()
    async create(@Body() createUserDto: CreateUserDto) {
        return await this.usersService.create(createUserDto);
    }

    @Get(':id')
    async getUser(@Param('id') id: number){
       return await this.usersService.findById(id); 
    }

    @Put(':id')
    async update(@Body() user: IUser, @Param('id') id: number): Promise<IUser> {
      return await this.usersService.update(id, user);
    }

    @Delete(':id')
    async deleteUser(@Param('id') id: number): Promise<string> {
      return await this.usersService.delete(id);
    }
    @Get('count/user')
    async getCountUser(): Promise<number> {
        return await this.usersService.countUser();
    }

    @Cron('*******')
    handleCron() {
        this.logger.debug('Called when the current second is 45'+ this.usersService.countUser());
      }
}
