import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { UsersService } from '../users/users.service';
import { JwtPayload} from './interfaces/jwt-payload.interface';
import { Model, PassportLocalModel } from 'mongoose';
import { IUser } from '../users/interfaces/user.interface';
import { InjectModel } from '@nestjs/mongoose';
import { debug } from 'console';
import { RegistrationStatus } from './interfaces/registrationStatus.interface';
import { use } from 'passport';
 
@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        @InjectModel('User') private readonly userModel: PassportLocalModel<IUser>
    ) {}

    async register(user: IUser) {
        let status: RegistrationStatus = { success: true, message: 'user register'};
        await this.userModel.register(new this.userModel({ 
            username: user.email,
            firstName: user.firstName,
            lastName: user.lastName }),
            user.password, 
            (err) => { 
            if( err ) {
                debug(err);
                status = { success: false, message: err };
            }
        });
        return status;
    }
    createToken(user) {
        console.log('get the expiration');
        const expiresIn = 3600;
        console.log('sign the Token');
        console.log(user);

        const accsessToken = jwt.sign({ id: user.id,email: user.username,firstname: user.firstname, lastname: user.lastName }, 'ILoveCoding', { expiresIn });
            console.log('return the Token');
            console.log(accsessToken);
            return { expiresIn, accsessToken,};
    }

    async validateUser(payload: JwtPayload): Promise<any> {
        const user = await this.userService.findById(payload.id);
        if( !user ) {
            return null;
        }
        return user;
    }
} 