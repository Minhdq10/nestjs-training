import { Module, NestModule,  MiddlewareConsumer, RequestMethod, } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
// Strategies
import { JwtStrategy } from './passport/jwt.strategy';
import { LocalStrategy } from './passport/local.strategy';
import { ConfigModule } from '@nestjs/config';
import { TasksService } from './taskService';


@Module({
  imports: [UsersModule,ConfigModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy, TasksService ],
  exports: [AuthService, JwtStrategy, LocalStrategy],
})
export class AuthModule {}
