import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { UsersService } from '../users/users.service';
import { number } from 'joi';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);
  constructor( private readonly usersService: UsersService,) {} 

  getCountUser(): Promise<number> {
      return this.usersService.countUser()
  }

  @Cron('45 * * * * *')
  handleCron(): Promise<number> {
     const numberOne = this.usersService.countUser();
    this.logger.debug('Called when the current second is 45'+ numberOne);
    return 
  }
}