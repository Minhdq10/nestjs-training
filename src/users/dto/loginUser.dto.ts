import { ApiProperty} from '@nestjs/swagger';


export class LoginUserDto {

    @ApiProperty()
    readonly email: string;

    @ApiProperty()
    readonly password: string;

}

/**
 * file Dto, use swagger, users can know what parameters to use when sending post requests
 */