import { Controller, Get, Post, Delete, Param, Body, Put, UseGuards , Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/createUser.dto';
import { UserSchema } from './schema/user.schema';
import { LocalStrategy } from '../auth/passport/local.strategy';
import { JwtAuthGuard } from "../auth/passport/jwt-auth.guard";

@Controller('users')
export class UsersController {
}
