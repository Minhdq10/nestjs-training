import * as mongoose from 'mongoose';
import * as passportLocalMongoose from 'passport-local-mongoose';

export const UserSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
});

UserSchema.plugin(passportLocalMongoose);

/**
 * PassportLocalMongoose this plugin  setup of hashing our passwords for MongoDB
 * Passport User Model
 */