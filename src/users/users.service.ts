import { Injectable, Catch } from '@nestjs/common';
import { Model, PassportLocalModel } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { debug } from 'console';
import { IUser } from './interfaces/user.interface';
import { CreateUserDto } from './dto/createUser.dto';
import { IUserService } from './interfaces/iusers.service';

@Injectable()
export class UsersService implements IUserService {
    constructor(@InjectModel('User') private readonly userModel: PassportLocalModel<IUser>) {}

    async findAll(): Promise<IUser[]> {
        return await this.userModel.find().exec();
    }

    async findOne(options: object): Promise<IUser> {
        return this.userModel.findOne(options).exec();
    }

    async findById(Id: number): Promise<IUser> {
        return await this.userModel.findById(Id).exec();
    }

    async create(createUserDto: CreateUserDto): Promise<IUser> {
        const creatUser = new this.userModel(createUserDto);
        return await creatUser.save();
    }

    async update(Id: number, newValue: IUser): Promise<IUser> {
        const user = await this.userModel.findById(Id).exec();
        if(!user._id) {
            debug('user not found');
        }
        await this.userModel.findByIdAndUpdate(Id, newValue).exec();
        return await this.userModel.findById(Id).exec();
    }

    async delete(Id: number): Promise<string> {
        try {
            await (await this.userModel.findByIdAndRemove(Id).exec());
            return "Delete success!";
        } catch (err) {
            debug(err);
            return "Delete fall";
        }
    }
    async countUser(): Promise<number> {
         return await this.userModel.estimatedDocumentCount();
    }
}
